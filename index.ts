import { Gitlab } from "@gitbeaker/node";
import { Types } from '@gitbeaker/node';
import * as c from './config.json';

const api = new Gitlab({
    host: 'http://gitlab.com',
    token: c.token,
})
async function main() {
    const [badger] = await (await api.Users.projects(4084970))
        .filter((e) => e.name === "Badger")
    const id = badger.id;
    const options: Types.BaseRequestOptions = {
        link_url: "https://gitlab.com/kristophermanceaux/badger",
        image_url: "https://img.shields.io/badge/-sdfgvdfv-blueviolet"
    }
    await api.ProjectBadges.add(id, options)
}
main();